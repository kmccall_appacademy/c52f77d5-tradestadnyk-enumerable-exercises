require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  if arr.empty?
    return 0
  end
  reduced = arr.reduce(:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? { |long_str_ele| sub_string?(long_str_ele, substring)  }
end

def sub_string?(long_string, substring)
  long_string.include?(substring)
end
# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  selected = string.split(" ").join("").chars.select {|ele| appear_more_than_once?(ele, string)}
  selected.uniq.sort
end

def appear_more_than_once?(char, string)
  counted = string.chars.count {|ele| ele == char}
  counted >= 2? true : false
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  string.split(" ").sort_by {|word| word.length}[-2..-1].sort
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  alphabet = "qwertyuioplkjhgfdsazxcvbnm".split("")
  result = alphabet - string.split("").uniq
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  reduced = (first_yr..last_yr).reduce([]) do |years, curr_year|
    if not_repeat_year?(curr_year)
      years << curr_year
    else
      years
    end
  end
  reduced
end

def not_repeat_year?(year)
  year.to_s.split('').uniq == year.to_s.split('')
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  reduced = songs.reduce([]) do |one_weekers, song|
    if !no_repeats?(song, songs)
      one_weekers << song
    else
      one_weekers
    end
  end
  reduced.uniq
end

def no_repeats?(song_name, songs)
  songs.each_index do |idx|
    curr_song = songs[idx]
    next_song = songs[idx + 1]
    if song_name == curr_song && (curr_song == next_song)
      return true
    end
  end
  false
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  removed_punc_string = remove_punctuation(string).split(' ')
  cs_word = ""
  c_dist = nil
  removed_punc_string.reverse.each do |ele|
    if (c_dist == nil) || (c_distance(ele) <= c_dist)
      c_dist = c_distance(ele)
      cs_word = ele
    end
  end
  cs_word
end

def c_distance(word)
  i = word.length - 1
  while i >= 0
    if word[i] == "c"
      return word.length - (i + 1)
    end
    i -= 1
  end
  word.length
end

def remove_punctuation(string)
  string.downcase.gsub(/[^a-z0-9\s]/i, '')
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  result = [0] #I did it , because in first if statement, I can not compare
                # nil to integer.
  arr.each_with_index do |ele, idx|
    if result.flatten.max > idx
      next
      #p arr.drop(idx)
    elsif arr[idx] == arr[idx + 1]
      result << find_end_index(idx, arr.drop(idx))
    end
  end
  result.drop(1) #at the end I am getting rid of first ([0]) element
end

def find_end_index(i, arr)
  count = 0
  arr.each_with_index do |ele, idx|
    curr = arr[idx]
    next_el = arr[idx + 1]
    if curr == next_el
      count += 1
    elsif curr != next_el
      return [i, i + count]
    end
  end
end
